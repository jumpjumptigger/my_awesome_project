from datetime import timedelta
class DevelopmentConfig():
  DEBUG = True
  SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:123456@127.0.0.1:3306/studyweb'
  SQLALCHEMY_TRACK_MODIFICATIONS = False
  SECRET_KEY = 'sdfaldk'
  ENV='development'
  SEND_FILE_MAX_AGE_DEFAULT = timedelta(seconds=1)