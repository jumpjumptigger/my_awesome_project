
from apps.createdb import db, UserInfoTable, ErrorListTable, DiscussTable, ReplyTable, \
    UserDataInfoTable, ProblemListTable, UserPublishListTable, ChapterListTable, KnowledgeListTable, IndependentListTable
from apps import DataType

def UserInfoAdd( Name, Password, Emial, Photo):
    oldUser = UserInfoTable.query.filter_by(name=Name).count()
    if oldUser != 0:
        return False
    else:
        user = UserInfoTable(name=Name, pwd=Password, email=Emial, photo=Photo, role='User')
        db.session.add(user)
        db.session.commit()
        return True

def UserInfoCheckForID(id):
    user = UserInfoTable.query.get(id)
    return user

def UserInfoChekForName(Name,password):
    user = UserInfoTable.query.filter_by(name=Name).all()
    for i in user:
        if i.pwd == password:
            DataType.SetUserInfo(i.id, i.name, i.pwd, i.email, i.photo, i.role)
            return True
    return False

def UserInfoUpdate(uID, Name, Password, Email, Photo):
    user = UserInfoTable.query.get(uID)
    user.name = Name
    user.pwd = Password
    user.email = Email
    user.photo = Photo
    db.session.commit()

def UserDataAdd(uid, Exercises, Correct, Accuracy, Gold, Likes, QuizzesUnLock):
    userData = UserDataInfoTable(Exercises=Exercises, Correct=Correct, Accuracy=Accuracy, Gold=Gold, Likes=Likes, QuizzesUnLock= QuizzesUnLock, UserID=uid)
    db.session.add(userData)
    db.session.commit()

def UserDataUpdate(uid, type, data):
    useData = UserDataInfoTable.query.filter_by(UserID=uid)
    for use in useData:
        if type == 'QuizzesUnLock':
            print('QuizzesUnLock')
            use.QuizzesUnLock = data
        elif type == 'Likes':
            use.Likes += data
        elif type == 'Exercises':
            use.Exercises += data
        elif type == 'Correct':
            use.Correct = data
        elif type == 'Accuracy':
            use.Accuracy = data
        elif type == 'Gold':
            use.Gold = data
    db.session.commit()

def ErrorListAdd(count, time, status, errid, uid):
    ErrList = ErrorListTable(Count=count, LateTime=time, ErrorStatus=status, ErrorID=errid, UserID=uid)
    db.session.add(ErrList)
    db.session.commit()


def ErrorListChange(id, time, count):
    error = ErrorListTable.query.get(id)
    print(error.Count)
    error.LateTime = time
    error.Count = count
    db.session.commit()


def ErrorListCheckCountByErrorID(errid):
    errorcount = ErrorListTable.query.filter_by(ErrorID=errid).count()
    return errorcount

def ErrorListCheckByErrorID(errid):
    error = ErrorListTable.query.filter_by(ErrorID=errid)
    return error


def DiscussListAdd(content,uid):
    dis = DiscussTable(Content=content, userID=uid)
    db.session.add(dis)
    db.session.commit()


def ReplyListAdd(disID, content, uid):
    rep = ReplyTable(DiscussID=disID, Content=content, ReUserID=uid)
    db.session.add(rep)
    db.session.commit()


def ChapterListUpdate(id, Fname, Sname):
    cha = ChapterListTable.query.get(id)
    cha.FirstDirectoryName = Fname
    cha.SecondDirectoryName = Sname
    db.session.commit()

def ChapterListAdd(Fnum, Fname, Snum, Sname):
    cha = ChapterListTable(FisrtDirectory=Fnum, FirstDirectoryName=Fname, SecondDirectory=Snum, SecondDirectoryName=Sname)
    db.session.add(cha)
    db.session.commit()

def KnowledgeListUpdate(id, example, solution, chaID):
    know = KnowledgeListTable.query.get(id)
    know.Example = example
    know.Solution = solution
    know.ChaID = chaID
    db.session.commit()

def KnowledgeListAdd(example, solution, chaID):
    know = KnowledgeListTable(Example=example, Solution=solution, ChaID=chaID)
    db.session.add(know)
    db.session.commit()

def ProblemListAdd(content, dis, answer, chapID, type, option):
    Pro = ProblemListTable(Type=type, Content=content, AnswerDes=dis, Answer=answer, ChapterID=chapID, Option=option)
    db.session.add(Pro)
    db.session.commit()

def ProblemListUpdate(ID, type, option, answer, answerdes,content):
    pro = ProblemListTable.query.get(ID)
    pro.Type = type
    pro.Option = option
    pro.Answer = answer
    pro.AnswerDes = answerdes
    pro.Content = content
    db.session.commit()

def UserPublishListDelete(id):
    pub = UserPublishListTable.query.get(id)
    db.session.delete(pub)
    db.session.commit()


def UserPublishListAdd(Content, Des, Answer):
    pub = UserPublishListTable(Answer=Answer, AnswerDes=Des, Content=Content)
    db.session.add(pub)
    db.session.commit()

def UserPublishListAdd(Content, answerdes, answer, uid):
    Flag = 0
    Publist = UserPublishListTable.query.all()
    for pub in Publist:
        if pub.Content == Content:
            Flag = 1
            break
    if Flag == 0:
        inde = UserPublishListTable(Answer=answer, AnswerDes=answerdes, Content=Content, Like=0, UserID=uid)
        db.session.add(inde)
        db.session.commit()

def UserPublishListUpdate(ID, Content, answerdes, answer):
    inde = UserPublishListTable.query.get(ID)
    inde.Answer = answer
    inde.AnswerDes = answerdes
    inde.Content = Content
    db.session.commit()

def IndependentListAdd(Content, uid):
    inde = IndependentListTable(Answer='', AnswerDes='', Content=Content, Like=0, UserID=uid)
    db.session.add(inde)
    db.session.commit()
