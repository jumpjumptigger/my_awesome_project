
class CurrentUserInfoType():
    id = ''
    name = ''
    password = ''
    email = ''
    photo = ''
    role = ''

global RankStatus
RankStatus = 0
global RmarkValue
RmarkValue = 0
currentUserInfo = CurrentUserInfoType()


def SetUserInfo(id, name, password, email, photo, role):
    currentUserInfo.id = id
    currentUserInfo.name = name
    currentUserInfo.password = password
    currentUserInfo.email = email
    currentUserInfo.photo = photo
    currentUserInfo.role = role

def ClearUserInfo():
    currentUserInfo.id = ''
    currentUserInfo.name = ''
    currentUserInfo.password = ''
    currentUserInfo.email = ''
    currentUserInfo.photo = ''

def GetUserInfo():
    return currentUserInfo

def SetRankStatus(value):
    global RankStatus
    RankStatus = value
def GetRankStatus():
    return RankStatus
def SetRmarkValue(value):
    global RmarkValue
    RmarkValue = value

def GetRmarkValue():
    return RmarkValue