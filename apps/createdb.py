from apps import app
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy(app)

class UserInfoTable(db.Model):
    __tablename__ = 'userInfo'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(10), nullable=False)
    pwd = db.Column(db.String(8), nullable=False)
    email = db.Column(db.String(20), nullable=True)
    photo = db.Column(db.String(50))
    role = db.Column(db.Enum('Admin', 'User'), nullable=False)
    Discuss = db.relationship('DiscussTable', backref='DiscussInfo')
    Reply = db.relationship('ReplyTable', backref='ReplyInfo')
    DataInfo = db.relationship('UserDataInfoTable', backref='DaInfo')

class UserDataInfoTable(db.Model):
    __tablename__ = 'userDataInfo'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    Exercises = db.Column(db.Integer, nullable=False)
    Correct = db.Column(db.Integer, nullable=False)
    Accuracy = db.Column(db.Float, nullable=False)
    Gold = db.Column(db.Integer, nullable=False)
    Likes = db.Column(db.Integer, nullable=False)
    QuizzesUnLock = db.Column(db.Integer, nullable=False)
    UserID = db.Column(db.Integer, db.ForeignKey('userInfo.id'))

class ChapterListTable(db.Model):
    __tablename__ = 'chapter'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    FisrtDirectory = db.Column(db.Integer, nullable=False)
    FirstDirectoryName = db.Column(db.String(50), nullable=False)
    SecondDirectory = db.Column(db.Integer, nullable=False)
    SecondDirectoryName = db.Column(db.String(50), nullable=False)
    Content = db.Column(db.Text, nullable=True)
    Example = db.Column(db.Text, nullable=True)
    ChaPro = db.relationship('ProblemListTable', backref="Chapter")
    Know = db.relationship('KnowledgeListTable', backref="Kno")

class KnowledgeListTable(db.Model):
    __tablename__ = 'knowledge'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    Example = db.Column(db.Text, nullable=False)
    Solution = db.Column(db.Text, nullable=False)
    ChaID = db.Column(db.Integer, db.ForeignKey('chapter.id'), nullable=False)

class ProblemListTable(db.Model):
    __tablename__ = 'problem'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    Type = db.Column(db.Enum('choice', 'ShortAnswer'), nullable=False)
    Option = db.Column(db.String(20), nullable=True)
    Answer = db.Column(db.String(50), nullable=False)
    AnswerDes = db.Column(db.String(200), nullable=True)
    Content = db.Column(db.Text, nullable=True)
    ChapterID = db.Column(db.Integer, db.ForeignKey('chapter.id'))
    ProblemErr = db.relationship('ErrorListTable', backref="pro")


class ErrorListTable(db.Model):
    __tablename__ = 'ErrorList'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    Count = db.Column(db.Integer, nullable=False)
    LateTime = db.Column(db.DateTime, nullable=True)
    ErrorStatus = db.Column(db.Enum('TRUE', 'FALSE'), nullable=False)
    ErrorID = db.Column(db.Integer, db.ForeignKey('problem.id'))
    UserID = db.Column(db.Integer, nullable=False)


class DiscussTable(db.Model):
    __tablename__ = 'discuss'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    userID = db.Column(db.Integer, db.ForeignKey('userInfo.id'), nullable=False)
    Content = db.Column(db.Text, nullable=True)
    ReplyRelationship = db.relationship('ReplyTable', backref="Dis")

class ReplyTable(db.Model):
    __tablename__ = 'reply'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    DiscussID = db.Column(db.Integer, db.ForeignKey('discuss.id'), nullable=False)
    Content = db.Column(db.Text, nullable=False)
    ReUserID = db.Column(db.Integer, db.ForeignKey('userInfo.id'), nullable=False)

class QuizzesListTable(db.Model):
    __tablename__ = 'quizzes'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    Level = db.Column(db.Integer, nullable=False)
    cost = db.Column(db.Integer, nullable=False)
    QuestionRelationship = db.relationship('QuizzesQuestionListTable', backref='qui')

class QuizzesQuestionListTable(db.Model):
    __tablename__ = 'question'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    Content = db.Column(db.String(250), nullable=False)
    Answer = db.Column(db.String(50), nullable=False)
    Quizzes = db.Column(db.Integer, db.ForeignKey('quizzes.id'), nullable=False)

class UserPublishListTable(db.Model):
    __tablename__ = 'Publish'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    Answer = db.Column(db.String(50), nullable=False)
    AnswerDes = db.Column(db.String(200), nullable=True)
    Content = db.Column(db.Text, nullable=True)
    Like = db.Column(db.Integer, nullable=False)
    UserID = db.Column(db.Integer, nullable=False)

class IndependentListTable(db.Model):
    __tablename__ = 'independent'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    Answer = db.Column(db.String(50), nullable=False)
    AnswerDes = db.Column(db.String(200), nullable=True)
    Content = db.Column(db.Text, nullable=True)
    Like = db.Column(db.Integer, nullable=False)
    UserID = db.Column(db.Integer, nullable=False)
#db.drop_all()
db.create_all()