from flask import request, render_template, Blueprint
from createdb import db, QuizzesListTable, QuizzesQuestionListTable

Blu_AdminQuizzes = Blueprint('Blu_AdminQuizzes', __name__)

@Blu_AdminQuizzes.route('/Admin_Quizzes', methods=['GET', 'POST'])
def Admin_Quizzes():
    OptionType = request.args.get('OptionType')
    if OptionType == None:
        OptionType = '1'
    OptionType = int(OptionType)
    if OptionType != 2:
        HTML = AdminQuizzesDisplay()
    else:
        HTML = AdminQuizzesModify()
    return HTML

def AdminQuizzesDisplay():
    QuizzesID = request.args.get('QuizzesID')
    if QuizzesID != None:
        QuizzesID = int(QuizzesID)
        Qui = QuizzesQuestionListTable.query.get(QuizzesID)
        db.session.delete(Qui)
        db.session.commit()
    if request.method == 'POST':
        print('asdfsdfs')
        Rcontent = request.form.get('Rcontent')
        Ranswer = request.form.get('Ranswer')
        Rlevel = request.form.get('Rlevel')
        Rlevel = int(Rlevel)
        if(Rlevel<=3):
            QuizzesAdd(Rcontent, Ranswer, Rlevel)
    ProblemList = QuizzesQuestionListTable.query.all()
    return render_template('Admin_Quizzes.html',
                           ProblemList=ProblemList,
                           OptionType=1)

def AdminQuizzesModify():
    QuizzesID = request.args.get('QuizzesID')
    QuizzesID = int(QuizzesID)
    ProblemList = QuizzesQuestionListTable.query.get(QuizzesID)

    if request.method == 'POST':
        Qcontent = request.form.get('Qcontent')
        Qanswer = request.form.get('Qanswer')
        Qlevel = request.form.get('Qlevel')

        ProblemList.Content = Qcontent
        ProblemList.Answer = Qanswer
        ProblemList.Quizzes = int(Qlevel)
        print(Qlevel)
        db.session.commit()
    return render_template('Admin_Quizzes.html',
                           ProblemList=ProblemList,
                           OptionType=2)

def QuizzesAdd(content, answer, level):
    Flag = 0
    QuiList = QuizzesQuestionListTable.query.all()
    for qui in QuiList:
        if qui.Content == content:
            Flag = 1
            break
    if Flag == 0:
        Qui = QuizzesQuestionListTable(Content=content, Answer=answer, Quizzes=level)
        db.session.add(Qui)
        db.session.commit()