from flask import render_template, Blueprint, request
from createdb import UserInfoTable, UserDataInfoTable, db, ErrorListTable, DiscussTable, ReplyTable
from flask_sqlalchemy import SQLAlchemy
from apps import oprate_sql

Blu_AdminUser = Blueprint('Blu_AdminUser', __name__)

@Blu_AdminUser.route('/Admin_user', methods=['POST', 'GET'])
def Admin_user():
    Option = request.args.get("OptionType")
    UserID = request.args.get("User_id")
    if (Option == None):
       HTML = OptionDisplay()
    elif int(Option) == 3:
        OptionDelete(UserID)
        HTML = OptionDisplay()
    elif int(Option) == 2:
        HTML = OptionModify(UserID)
    return HTML


def OptionDisplay():
    UserList = UserInfoTable.query.all()
    print(UserList)
    return render_template('Admin_user.html',
                           UserList=UserList,
                           OptionType=1)

def OptionDelete(uID):
    UserData = UserDataInfoTable.query.all()
    uID = int(uID)
    for ud in UserData:
        if ud.UserID == int(uID):
            db.session.delete(ud)
            db.session.commit()

    ErrList = ErrorListTable.query.filter_by(UserID=uID)
    for err in ErrList:
        db.session.delete(err)
        db.session.commit()

    ReplyList = ReplyTable.query.filter_by(ReUserID=uID)
    for re in ReplyList:
        db.session.delete(re)
        db.session.commit()

    DisList = DiscussTable.query.filter_by(userID=uID)
    for dis in DisList:
        db.session.delete(dis)
        db.session.commit()
    UserInfo = UserInfoTable.query.get(int(uID))
    db.session.delete(UserInfo)
    db.session.commit()

def OptionModify(uID):
    uID = int(uID)

    UserInfo = UserInfoTable.query.get(uID)
    print(UserInfo)
    if request.method == 'POST':
        print('Submit')
        name = request.form.get('Name')
        pwd = request.form.get('Pwd')
        email = request.form.get('Email')
        print(name)
        print(pwd)
        print(email)
        oprate_sql.UserInfoUpdate(uID, name, pwd, email, UserInfo.photo)
    return render_template('Admin_user.html',
                           UserInfo=UserInfo,
                           OptionType=2)