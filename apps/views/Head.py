from flask import request, render_template, Blueprint, redirect
from apps import DataType, oprate_sql
from createdb import UserDataInfoTable
Blu_Header = Blueprint('Blu_Header', __name__)

@Blu_Header.route('/Header')
def Header():
    uid = DataType.GetUserInfo().id
    uid = str(uid)
    if (len(uid) == 0):
        return redirect('/Login')
    uid = int(uid)
    ConverStatus = request.args.get("ConverStatus")
    if ConverStatus != None:
        ConverStatus = int(ConverStatus)
        if ConverStatus == 1:
            UserDataList = UserDataInfoTable.query.filter_by(UserID=uid)
            for us in UserDataList:
                if us.Likes >=1:
                    oprate_sql.UserDataUpdate(uid, 'Likes', -1)
                    oprate_sql.UserDataUpdate(uid, 'Gold', us.Gold+10)
                else:
                    ConMsg = 'Unable to convert'
    userInfo = oprate_sql.UserInfoCheckForID(uid)
    UserDataList = UserDataInfoTable.query.filter_by(UserID=uid)

    for us in UserDataList:
        UserDataInfo = us
    return render_template('Header.html',
                           UserInfo=userInfo,
                           UserDataInfo=UserDataInfo,
                           RankStatus=DataType.GetRankStatus(),
                           RankValue=DataType.GetRmarkValue(),
                           )