from flask import request, render_template, Blueprint
from createdb import ChapterListTable, KnowledgeListTable, db
from apps import oprate_sql
Blu_AdminKnowledge = Blueprint('Blu_AdminKnowledge', __name__)

@Blu_AdminKnowledge.route('/Admin_Knowledge', methods=['POST', 'GET'])
def Admin_Knowledge():
    OptionType = request.args.get('OptionType')
    if OptionType == None:
        OptionType = '1'
    OptionType = int(OptionType)
    if OptionType == 2:
        KnowID = request.args.get('KnowID')
        KnowID = int(KnowID)
        HTML = ChapterListUpdate(KnowID)
    elif OptionType == 3:
        KnowID = request.args.get('KnowID')
        KnowID = int(KnowID)
        ChapterDelete(KnowID)
        HTML = ChapterDisplayInfo()
    elif OptionType == 4:
        KnowID = request.args.get('KnowID')
        KnowID = int(KnowID)
        HTML = KnowledgeListUpdate(KnowID)
    elif OptionType == 5:
        KnowID = request.args.get('KnowID')
        KnowID = int(KnowID)
        KnowledgeDelete(KnowID)
        HTML = ChapterDisplayInfo()
    else:
        HTML = ChapterDisplayInfo()
    return HTML

def ChapterDisplayInfo():
    if request.method == 'POST':
        InputType = request.form.get('Type')
        if InputType == 'Chapter':
            FirstNum = request.form.get('FirstNum')
            Firstname = request.form.get('Firstname')
            SecondNum = request.form.get('SecondNum')
            Secondname = request.form.get('Secondname')
            oprate_sql.ChapterListAdd(FirstNum, Firstname, SecondNum, Secondname)
        elif InputType == 'Knowledge':
            Iexaplem = request.form.get('Iexaplem')
            Isolution = request.form.get('Isolution')
            Ichapterid = request.form.get('Ichapterid')
            oprate_sql.KnowledgeListAdd(Iexaplem, Isolution, Ichapterid)
    ChapterList = ChapterListTable.query.all()
    KnowledgeList = KnowledgeListTable.query.all()
    return render_template('Admin_Knowledge.html',
                           ChapterList=ChapterList,
                           KnowledgeList=KnowledgeList,
                           OptionType=1)

def ChapterDelete(id):
    ChaList = ChapterListTable.query.get(id)
    for kno in ChaList.Know:
        db.session.delete(kno)
        db.session.commit()
    for pro in ChaList.ChaPro:
        db.session.delete(pro)
        db.session.commit()
    db.session.delete(ChaList)
    db.session.commit()

def KnowledgeDelete(id):
    Know = KnowledgeListTable.query.get(id)
    db.session.delete(Know)
    db.session.commit()

def ChapterListUpdate(id):
    if request.method == 'POST':
        ChaID = request.form.get('ID')
        Fname = request.form.get('Fname')
        Sname = request.form.get('Sname')
        oprate_sql.ChapterListUpdate(id, Fname, Sname)
    ChapterList = ChapterListTable.query.get(id)
    return render_template('Admin_Knowledge.html',
                           ChapterList=ChapterList,
                           OptionType=2)

def KnowledgeListUpdate(id):
    if request.method == 'POST':
        ID = request.form.get('ID')
        Example = request.form.get('Example')
        Solution = request.form.get('Solution')
        ChaID = request.form.get('ChaID')
        oprate_sql.KnowledgeListUpdate(id, Example, Solution, ChaID)
    KnowledgeList = KnowledgeListTable.query.get(id)
    return render_template('Admin_Knowledge.html',
                           KnowledgeList=KnowledgeList,
                           OptionType=3)