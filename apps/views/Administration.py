from flask import request, render_template, Blueprint, redirect
from apps import DataType
Blu_Admin = Blueprint('Blu_Admin', __name__)

@Blu_Admin.route('/Administration')
def Administration():
    uid = DataType.GetUserInfo().id
    uid = str(uid)
    if (len(uid) == 0):
        return redirect('/Login')
    OpID = request.args.get('OptionID')
    if OpID == None:
        OpID = 1
    else:
        OpID = int(OpID)
    return render_template('Administration.html',
                           OptionID=OpID)