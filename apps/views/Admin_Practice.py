from flask import request, render_template, Blueprint
from createdb import ProblemListTable, ChapterListTable, db
from apps import oprate_sql
Blu_AdminPractice = Blueprint('Blu_AdminPractice', __name__)
@Blu_AdminPractice.route('/Admin_Practice', methods=['POST', 'GET'])
def Admin_Practice():
    DeleteID = request.args.get('DeleteID')
    OptionType = request.args.get('OptionType')
    ModifyID = request.args.get('ModifyID')
    if OptionType == None:
        OptionType='1'
    OptionType = int(OptionType)
    if OptionType != 2:
        HTML = DisplayPracticeInfo(DeleteID)
    elif OptionType == 2:
        HTML = ModifyPracticeInfo(ModifyID)
    return HTML

def DisplayPracticeInfo(DeleteID):
    if DeleteID != None:
        DeleteID = int(DeleteID)
        de = ProblemListTable.query.get(DeleteID)
        db.session.delete(de)
        db.session.commit()
    ProList = ProblemListTable.query.all()
    if request.method == 'POST':
        FirstDire = request.form.get('FirstDire')
        SecondDire = request.form.get('SecondDire')
        Content = request.form.get('Content')
        Detail = request.form.get('Detail')
        Answer = request.form.get('Answer')
        Type = request.form.get('Type')
        Option = request.form.get('Option')
        print(Answer)
        CharList = ChapterListTable.query.all()
        if (FirstDire != None) and (SecondDire != None) and (Content != None) and (Detail != None) and (Answer != None):
            for cha in CharList:
                if (cha.FisrtDirectory == int(FirstDire)) and (cha.SecondDirectory == int(SecondDire)):
                    print(FirstDire)
                    oprate_sql.ProblemListAdd(Content, Detail, Answer, cha.id, Type, Option)
    return render_template('Admin_Practice.html',
                           PublishList=ProList,
                           OptionType=1)
def ModifyPracticeInfo(id):
    ModifyID = int(id)

    pra = ProblemListTable.query.get(id)
    print(pra)
    if request.method == 'POST':
        Id = request.form.get('Id')
        Type = request.form.get('Type')
        Option = request.form.get('Option')
        Answer = request.form.get('Answer')
        AnswerDes = request.form.get('AnswerDes')
        Content = request.form.get('Content')
        oprate_sql.ProblemListUpdate(ModifyID, Type, Option, Answer, AnswerDes, Content)
    return render_template('Admin_Practice.html',
                           PublishList=pra,
                           OptionType=2)