from flask import render_template, Blueprint, request, redirect
from apps.createdb import ErrorListTable, UserDataInfoTable
from apps import oprate_sql, DataType
Blu_Error = Blueprint("Blu_Error", __name__)


@Blu_Error.route('/Error')
def Error():

    Singout = request.args.get('Singout')
    if Singout != None:
        if int(Singout) == 1:
            DataType.ClearUserInfo()
    uid = DataType.GetUserInfo().id
    uid = str(uid)
    if (len(uid) == 0):
        return redirect('/Login')
    uid = int(uid)
    userInfo = oprate_sql.UserInfoCheckForID(uid)
    UserDataList = UserDataInfoTable.query.filter_by(UserID=uid)
    for us in UserDataList:
        UserDataInfo = us
    E_Count = ErrorListTable.query.filter_by(UserID=uid).count()
    E_List = ErrorListTable.query.filter_by(UserID=uid)
    E_Detail = request.args.get('Errir_id')
    if E_Detail==None:
        E_Detail=0
    print(E_Detail)
    Error_List = [[] for i in range(E_Count)]

    if E_Count == 0:
        Error_List = [[] for i in range(1)]
        Error_List[0].append(1)
        Error_List[0].append(' ')
        Error_List[0].append(' ')
        Error_List[0].append(' ')
        Error_List[0].append(' ')

    for lt, i in zip(E_List, range(E_Count)):
        Error_List[i].append(i+1)
        Error_List[i].append(lt.pro.Content)
        Error_List[i].append(lt.LateTime)
        Error_List[i].append(lt.ErrorID)
        Error_List[i].append(lt.pro.AnswerDes)
        print(Error_List)
    return render_template("Error.html",
                           Error_list=Error_List,
                           Error_count=E_Count,
                           Err_id=int(E_Detail),
                           UserInfo=userInfo,
                           UserDataInfo=UserDataInfo,
                           RankStatus=DataType.GetRankStatus(),
                           RankValue=DataType.GetRmarkValue())