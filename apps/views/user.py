
from flask import Blueprint, request, redirect, render_template
from sqlalchemy import asc, desc
import math
from apps import oprate_sql
from apps import DataType
from apps.createdb import UserDataInfoTable, UserInfoTable
Blu_user = Blueprint("Blu_user", __name__)

CurrentUser = dict()
@Blu_user.route('/Login', methods=["GET", "POST"])
def Login():
    if request.method == 'GET':
        return render_template('Login.html')
    user = request.form.get('username')
    pwd = request.form.get('password')
    temp = oprate_sql.UserInfoChekForName(user, pwd)
    if not temp is None:
        if temp == True:
            return redirect('/Home')

    return render_template('Login.html', msg='Username or password is incorrect')


@Blu_user.route('/Register', methods=['GET', 'POST'])
def Register():
    if request.method == 'GET':
        return render_template('Register.html')
    
    Uname = request.form.get('rename')
    Upwd = request.form.get('repwd')
    Uemail = request.form.get('remail')

    temp = oprate_sql.UserInfoAdd(Uname, Upwd, Uemail, None)

    UserInfo = UserInfoTable.query.all()

    for use in UserInfo:
        if (use.name==Uname) and (use.pwd==Upwd):
            oprate_sql.UserDataAdd(use.id, 0, 0, 0, 0, 0 , 0)
    if temp == True:
        return redirect('/Login')
    else:
        return render_template('Register.html', msg='User already exists')

@Blu_user.route('/Home')
def Home():

    Singout = request.args.get('Singout')
    if Singout != None:
        if int(Singout) == 1:
            DataType.ClearUserInfo()
    uid = DataType.GetUserInfo().id
    uid = str(uid)
    if (len(uid) == 0):
        return redirect('/Login')
    uid = int(uid)
    ConMsg = ''

    userInfo = oprate_sql.UserInfoCheckForID(uid)
    UserDataList = UserDataInfoTable.query.filter_by(UserID=uid)
    for us in UserDataList:
        UserDataInfo = us
    Ucount = UserInfoTable.query.filter_by(role='User')
    UserDataInfoCount = 0
    for uc in Ucount:
        UserDataInfoCount +=1
    UserDataInfoDec = UserDataInfoTable.query.order_by(desc('Gold'))
    RandList = [[] for i in range(UserDataInfoCount)]
    i = 0
    for us in UserDataInfoDec:
        usIn = UserInfoTable.query.get(us.UserID)
        if usIn.role == 'User':
            RandList[i].append(us.UserID)
            RandList[i].append(us.Gold)
            RandList[i].append(us.DaInfo.name)
            i += 1
    if UserDataInfo.Gold >= RandList[int(UserDataInfoCount/2)-1][1]:
        RankStatus = 1
    else:
        RankStatus = 2
    DataType.SetRankStatus(RankStatus)
    RankValue = abs(UserDataInfo.Gold - RandList[int(UserDataInfoCount/2)][1])
    DataType.SetRmarkValue(RankValue)
    return render_template('Home.html',
                           UserInfo=userInfo,
                           UserDataInfo=UserDataInfo,
                           RandList=RandList,
                           RankStatus=DataType.GetRankStatus(),
                           RankValue=DataType.GetRmarkValue(),
                           ConMsg=ConMsg)
