from flask import Flask, render_template, Blueprint, request, redirect
from apps.createdb import ChapterListTable, UserDataInfoTable, KnowledgeListTable
from apps import DataType, oprate_sql
Blu_study = Blueprint("Blu_study", __name__)


global ChoiceID
ChoiceID = 0
@Blu_study.route('/Study')
def Study():

    Singout = request.args.get('Singout')
    ChoicID = request.args.get('ChoiceID')
    if ChoicID != None:
        global ChoiceID
        ChoiceID = int(ChoicID)
    if Singout != None:
        if int(Singout) == 1:
            DataType.ClearUserInfo()
    uid = DataType.GetUserInfo().id
    uid = str(uid)
    if (len(uid) == 0):
        return redirect('/Login')
    uid = int(uid)
    userInfo = oprate_sql.UserInfoCheckForID(uid)
    UserDataList = UserDataInfoTable.query.filter_by(UserID=uid)
    for us in UserDataList:
        UserDataInfo = us

    SencArry = []
    chapterList = ChapterListTable.query.all()
    chap_id = request.args.get("chapter_id")
    exNumber = request.args.get('exNumber')
    if exNumber == None:
        exNumber = '1'
    exNumber = int(exNumber)
    if chap_id==None:
        chap_id=1
    else:
        chap_id = int(chap_id)
    for i in chapterList:
        if len(SencArry) < i.FisrtDirectory:
            SencArry.append(0)
        SencArry[i.FisrtDirectory-1] = i.FirstDirectoryName
    FisrtCount = len(SencArry)
    ExampleList = KnowledgeListTable.query.filter_by(ChaID=chap_id)
    Count = KnowledgeListTable.query.filter_by(ChaID=chap_id).count()
    if exNumber >= Count:
        exNumber = Count
    elif exNumber <= 1:
        exNumber = 1

    return render_template('Study.html',
                           chapter=chapterList,
                           FirstDreCount=FisrtCount,
                           SecDre=SencArry,
                           chapid=chap_id,
                           UserInfo=userInfo,
                           UserDataInfo=UserDataInfo,
                           RankStatus=DataType.GetRankStatus(),
                           RankValue=DataType.GetRmarkValue(),
                           ChoiceID=ChoiceID,
                           ExampleList=ExampleList,
                           ExampleNum=exNumber)