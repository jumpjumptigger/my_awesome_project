from flask import Blueprint, request, render_template, redirect
from apps.createdb import DiscussTable, ReplyTable, UserDataInfoTable
from apps import DataType
from apps import oprate_sql

Blu_Discuss = Blueprint('Blu_Discuss', __name__)
global ReplayId
ReplayId = 0

@Blu_Discuss.route('/Discuss', methods=['POST', 'GET'])
def Discuss():
    Singout = request.args.get('Singout')
    ReplayID = request.args.get('ReplayID')
    if ReplayID != None:
        ReplayID = int(ReplayID)
    global ReplayId
    if ReplayId == ReplayID:
        ReplayId = 0
    else:
        ReplayId = ReplayID
    if Singout != None:
        if int(Singout) == 1:
            DataType.ClearUserInfo()
    uid = DataType.GetUserInfo().id
    uid = str(uid)
    if (len(uid) == 0):
        return redirect('/Login')
    uid = int(uid)
    userInfo = oprate_sql.UserInfoCheckForID(uid)
    UserDataList = UserDataInfoTable.query.filter_by(UserID=uid)
    for us in UserDataList:
        UserDataInfo = us
    pubcontent = request.args.get("PubContent")
    replycotent = request.args.get("ReplyContent")
    disID = request.args.get("Submit")
    LikesUserID = request.args.get("LikesResid")
    uid = int(uid)
    if (replycotent != None) and (len(replycotent) != 0):
        if(disID != None) and (len(disID) != 0):
            disID = int(disID)
            oprate_sql.ReplyListAdd(disID, replycotent, uid)
    if (pubcontent != None) and (len(pubcontent) != 0):
        oprate_sql.DiscussListAdd(pubcontent, uid)
    DiscussList = DiscussTable.query.all()
    if LikesUserID != None:
        LikesUserID = int(LikesUserID)
        if LikesUserID != uid:
            oprate_sql.UserDataUpdate(LikesUserID, 'Likes', 1)
    return render_template('Discuss.html',
                           HTML_DiscussList=DiscussList,
                           UserInfo=userInfo,
                           UserDataInfo=UserDataInfo,
                           RankStatus=DataType.GetRankStatus(),
                           RankValue=DataType.GetRmarkValue(),
                           ReplayID=ReplayId)
