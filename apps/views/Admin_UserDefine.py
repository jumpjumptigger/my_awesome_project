from flask import request, render_template, Blueprint, redirect
from apps.createdb import UserDataInfoTable, UserPublishListTable, ChapterListTable, db
from apps import DataType, oprate_sql

Blu_AdminUserDefine = Blueprint('Blu_AdminUserDefine', __name__)

@Blu_AdminUserDefine.route('/Admin_UserDfine', methods=['GET', 'POST'])
def Admin_UserDfine():
    uid = DataType.GetUserInfo().id
    uid = str(uid)
    uid = int(uid)
    userInfo = oprate_sql.UserInfoCheckForID(uid)
    UserDataList = UserDataInfoTable.query.filter_by(UserID=uid)
    for us in UserDataList:
        UserDataInfo = us

    OptionType = request.args.get('OptionType')
    if OptionType == None:
        OptionType = '1'
    OptionType = int(OptionType)
    if OptionType != 2:
        HTML = PostQuestionDisplay(userInfo, UserDataInfo)
    else:
        HTML = ModifyPostQuestion(userInfo, UserDataInfo)
    return HTML

def PostQuestionDisplay(userInfo, UserDataInfo):
    PubId = request.args.get('PublishID')
    if PubId != None:
        PubId = int(PubId)
        print(PubId)
        PubList = UserPublishListTable.query.get(PubId)
        print(PubList.Content)
        db.session.delete(PubList)
        db.session.commit()
        '''
        CharList = ChapterListTable.query.all()
        for cha in CharList:
            if (cha.FisrtDirectory == PubList.FisrtDirectory) and (cha.SecondDirectory == PubList.SecondDirectory):
                oprate_sql.ProblemListAdd(PubList.Content, PubList.AnswerDes, PubList.Answer, cha.id)
                #oprate_sql.UserPublishListDelete(PubId)
        '''
    Publish = UserPublishListTable.query.all()
    if request.method == 'POST':
        FirstDire = request.form.get('FirstDire')
        SecondDire = request.form.get('SecondDire')
        Content = request.form.get('Content')
        Detail = request.form.get('Detail')
        Answer = request.form.get('Answer')
        print(Answer)
        CharList = ChapterListTable.query.all()
        if(FirstDire != None) and (SecondDire != None) and (Content != None) and (Detail != None) and (Answer != None):
            for cha in CharList:
                if (cha.FisrtDirectory == int(FirstDire)) and (cha.SecondDirectory == int(SecondDire)):
                    print(FirstDire)
                    oprate_sql.ProblemListAdd(Content, Detail, Answer, cha.id)
    return render_template('Admin_UserDefine.html',
                           UserInfo=userInfo,
                           UserDataInfo=UserDataInfo,
                           PublishList=Publish,
                           OptionType=1)

def ModifyPostQuestion(userInfo, UserDataInfo):
    PubId = request.args.get('PublishID')
    PubId = int(PubId)

    if request.method == 'POST':
        PubList = UserPublishListTable.query.get(PubId)
        Pcontent = request.form.get('Pcontent')
        Pdetail = request.form.get('Pdetail')
        Panswer = request.form.get('Panswer')

        PubList.Content = Pcontent
        PubList.AnswerDes = Pdetail
        PubList.Answer = Panswer
        db.session.commit()
    PubList = UserPublishListTable.query.get(PubId)

    return render_template('Admin_UserDefine.html',
                           UserInfo=userInfo,
                           UserDataInfo=UserDataInfo,
                           PublishList=PubList,
                           OptionType=2)

