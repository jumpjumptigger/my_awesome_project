from flask import request, render_template, Blueprint, redirect
from apps import DataType, oprate_sql
from apps.createdb import UserDataInfoTable, DiscussTable, ReplyTable, ChapterListTable, UserPublishListTable
import tkinter as tk
from tkinter import filedialog

Blu_Personal = Blueprint('Blu_Personal', __name__)




@Blu_Personal.route('/Personal', methods=['POST', 'GET'])
def Personal():
    uid = DataType.GetUserInfo().id
    uid = str(uid)
    if (len(uid)==0):
        return redirect('/Login')
    uid = int(uid)
    userInfo = oprate_sql.UserInfoCheckForID(uid)
    userDataList = UserDataInfoTable.query.filter_by(UserID=uid)
    for ud in userDataList:
        userData = ud
    DisType = request.args.get('DisplayInfo')
    DisTypeH = 0
    DisInfo = None
    if DisType != None:
        DisTypeH = int(DisType)
        if DisTypeH == 2:
            DisInfo = ReplyTable.query.filter_by(ReUserID=uid)
            print('1245678')
            for di in DisInfo:
                print(di.Dis)
        elif DisTypeH == 1:
            DisInfo = DiscussTable.query.filter_by(userID=uid)
    else:
        DisInfo = DiscussTable.query.filter_by(userID=uid)
    if request.method == 'POST':
        FirstDire = request.form.get('FirstDire')
        SecondDire = request.form.get('SecondDire')
        Content = request.form.get('Content')
        Detail = request.form.get('Detail')
        Answer = request.form.get('Answer')
        print(Answer)
        CharList = ChapterListTable.query.all()
        if(FirstDire != None) and (SecondDire != None) and (Content != None) and (Detail != None) and (Answer != None):
            for cha in CharList:
                if (cha.FisrtDirectory == int(FirstDire)) and (cha.SecondDirectory == int(SecondDire)):
                    print(FirstDire)
                    oprate_sql.UserPublishListAdd(int(FirstDire), int(SecondDire), Content, Detail, Answer)
    PublishList = UserPublishListTable.query.all()
    return render_template('Personal.html',
                           UserInfo=userInfo,
                           UserData=userData,
                           DisplayInfo=DisInfo,
                           DisplayType=DisTypeH,
                           RankStatus=DataType.GetRankStatus(),
                           RankValue=DataType.GetRmarkValue(),
                           PublishList=PublishList)