from flask import request, render_template, Blueprint, redirect
from createdb import ProblemListTable, ChapterListTable, ErrorListTable, UserDataInfoTable
from apps import oprate_sql, DataType
from time import strftime, gmtime
Blu_practice = Blueprint("Blu_practice", __name__)

SaveInfo = [1, 1]
global ChoiceID
ChoiceID = 0

global sub_result
sub_result = ''
global LastNum
LastNum = 0

@Blu_practice.route('/Practice', methods=['POST','GET'])
def Practice():

    Singout = request.args.get('Singout')
    if Singout != None:
        if int(Singout) == 1:
            DataType.ClearUserInfo()
    ChoicID = request.args.get('ChoiceID')
    if ChoicID != None:
        global ChoiceID
        ChoiceID = int(ChoicID)
    uid = DataType.GetUserInfo().id
    uid = str(uid)
    if (len(uid) == 0):
        return redirect('/Login')
    uid = int(uid)
    userInfo = oprate_sql.UserInfoCheckForID(uid)
    UserDataList = UserDataInfoTable.query.filter_by(UserID=uid)
    for us in UserDataList:
        UserDataInfo = us
    SencArry = []

    Errorid = request.args.get('Practice_id')
    if Errorid != None:
        Errorid = int(Errorid)
        SaveInfo[0] = ProblemListTable.query.get(Errorid).ChapterID
        pro_list = ProblemListTable.query.filter_by(ChapterID=SaveInfo[0])
        i = 0
        for pro in pro_list:
            if Errorid == pro.id:
                i += 1
                SaveInfo[1] = i
            else:
                i += 1
    pro_id = request.args.get("Problem_id")
    pro_number = request.args.get("Problem_number")
    if pro_id == None:
        pro_id = SaveInfo[0]
    if pro_number == None:
        pro_number = SaveInfo[1]

    SaveInfo[1] = pro_number
    SaveInfo[0] = pro_id
    pronum = int(pro_number)

    CharpterList = ChapterListTable.query.all()
    ProblemList = ProblemListTable.query.filter_by(ChapterID=pro_id)
    totalcount = ProblemListTable.query.filter_by(ChapterID=pro_id).count()
    if pronum >= totalcount:
        pronum = totalcount
    elif pronum <= 1:
        pronum = 1
    print(CharpterList)
    for i in CharpterList:
        print('SencArry:%s'%SencArry)
        print('FirstDire:%s'%i.FisrtDirectory)
        print('SecLen:%d'%len(SencArry))
        if len(SencArry) < i.FisrtDirectory:
            SencArry.append(0)
        SencArry[i.FisrtDirectory - 1] = i.FirstDirectoryName
    FisrtCount = len(SencArry)


    global sub_result
    global LastNum
    if LastNum != pronum:
        sub_result = ''
    LastNum = pronum
    if ProblemList[pronum-1].Type == 'choice':
        sub_result = request.args.get("ChoiceAnswer")
        if ProblemList[pronum-1].Answer == sub_result:
            Cal_result = "Correct"
        elif sub_result==None:
            Cal_result = ''
        else:
            Cal_result = "Wrong"
    else:
        sub_result = request.form.get('answer')
        if ProblemList[pronum-1].Answer == sub_result:
            Cal_result = "Correct"
        elif sub_result==None:
            Cal_result = ''
        else:
            Cal_result = "Wrong"

    if DataType.GetUserInfo().role == 'User':
        if Cal_result == "Wrong":
            print('Wrong')
            oprate_sql.UserDataUpdate(uid, 'Exercises', 1)
            oprate_sql.UserDataUpdate(uid, 'Accuracy', int((UserDataInfo.Correct*100)/(UserDataInfo.Exercises+1)))
            UpdateErrorList(ProblemList[pronum-1].id, uid)
        elif Cal_result == 'Correct':
            print('Correct')
            oprate_sql.UserDataUpdate(uid, 'Exercises', 1)
            oprate_sql.UserDataUpdate(uid, 'Correct', UserDataInfo.Correct + 1)
            oprate_sql.UserDataUpdate(uid, 'Gold', UserDataInfo.Gold + 1)
            oprate_sql.UserDataUpdate(uid, 'Accuracy', int(((UserDataInfo.Correct+1) * 100) / (UserDataInfo.Exercises + 1)))
    UserDataList = UserDataInfoTable.query.filter_by(UserID=uid)
    for us in UserDataList:
        UserDataInfo = us
        print(UserDataInfo.Accuracy)
    print(sub_result)
    return render_template('Practice.html',
                           chapter=CharpterList,
                           FirstDreCount=FisrtCount,
                           SecDre=SencArry,
                           chapid=int(pro_id),
                           pro_num=int(pronum),
                           problem=ProblemList,
                           submitResult=Cal_result,
                           UserInfo=userInfo,
                           RankStatus=DataType.GetRankStatus(),
                           RankValue=DataType.GetRmarkValue(),
                           ChoiceID=ChoiceID,
                           Canswer=sub_result,
                           UserDataInfo=UserDataInfo)


def UpdateErrorList(errid, uid):
    err = oprate_sql.ErrorListCheckCountByErrorID(errid)
    time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    print(time)
    print(errid)
    print(uid)
    checkStatus = 0
    if err==0:
        print('err==0')
        oprate_sql.ErrorListAdd(1, time, 'TRUE', errid, uid)
    else:
        error = ErrorListTable.query.filter_by(ErrorID=errid)
        for i in error:
            print(i.UserID)
            if i.UserID == uid:
                print('i.UserID == uid')
                oprate_sql.ErrorListChange(i.id, time, i.Count+1)
                checkStatus = 0
                break
            else:
                checkStatus = 1
        if checkStatus == 1:
            print('i.UserID != uid')
            oprate_sql.ErrorListAdd(1, time, 'TRUE', errid, uid)