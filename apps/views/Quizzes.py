from flask import Blueprint, render_template, request, redirect
from apps.createdb import QuizzesListTable, QuizzesQuestionListTable, UserDataInfoTable
from apps import DataType, oprate_sql
import random

Blu_Quizzes = Blueprint('Blu_Quizzes', __name__)

QuizzesInfo = [1, 1, 0]

DoCount = [0 for i in range(3)]

PrePorblemList = [[0 for i in range(10)] for j in range(3)]
global LockStatus
LockStatus = 0
@Blu_Quizzes.route('/Quizzes', methods=['GET', 'POST'])
def Quizzes():
    global LockStatus

    print(PrePorblemList)
    Singout = request.args.get('Singout')
    if Singout != None:
        if int(Singout) == 1:
            DataType.ClearUserInfo()
    uid = DataType.GetUserInfo().id
    uid = str(uid)
    if (len(uid) == 0):
        return redirect('/Login')
    uid = int(uid)
    userInfou = oprate_sql.UserInfoCheckForID(uid)
    userInfo = UserDataInfoTable.query.filter_by(UserID=uid)

    QuizeID = request.args.get('QuizzesID')
    Answer = request.form.get('QuiAnswer')
    ResLockStatus = request.args.get('QuizzesLockStatus')

    CosMsg = ''
    UserDataInfo=[]
    if (ResLockStatus != None) and (request.method != 'POST'):
        ResLockStatus = int(ResLockStatus)
        ResLockStatus = LockStatus | (1<<(ResLockStatus-1))
        Quid = int(QuizeID)
        Cost= QuizzesListTable.query.get(Quid).cost
        for da in userInfo:
            UserDataInfo = da
            if da.Gold >= Cost:
                oprate_sql.UserDataUpdate(uid, 'QuizzesUnLock', ResLockStatus)
                oprate_sql.UserDataUpdate(uid, 'Gold', da.Gold-Cost)
            else:
                CosMsg='Warning:You donnot have enough game coins to continue. Keep trying!'
    if QuizeID == None:
        QuizeID = QuizzesInfo[0]
    QuizeID = int(QuizeID)
    QuizzesInfo[0] = QuizeID

    QuiList = QuizzesListTable.query.all()
    QuiListCount = len(QuiList)
    QuestionList = QuizzesQuestionListTable.query.filter_by(Quizzes=QuizeID)
    QuestionCount = QuizzesQuestionListTable.query.filter_by(Quizzes=QuizeID).count()


    ErrorStatus = ''

    if Answer == None:
        print('Count:%d' % QuestionCount)
        QuestionNum = CreateProblemNumber(QuestionCount, QuizeID-1)
        QuizzesInfo[1] = QuestionNum
        SetProList((QuizeID-1), DoCount[QuizeID-1], QuestionNum)
    else:
        QuestionNum = QuizzesInfo[1]
        print('Answer:%s'%Answer)
        for da in userInfo:
            UserDataInfo = da
        if QuestionList[QuestionNum - 1].Answer != Answer:
            DoCountAdd(QuizeID - 1)
            oprate_sql.UserDataUpdate(uid, 'Exercises', 1)
            oprate_sql.UserDataUpdate(uid, 'Accuracy', int((UserDataInfo.Correct * 100) / (UserDataInfo.Exercises + 1)))
            ErrorStatus = 'Error'
        else:
            DoCountAdd(QuizeID - 1)
            ErrorStatus = 'Correct'
            oprate_sql.UserDataUpdate(uid, 'Exercises', 1)
            oprate_sql.UserDataUpdate(uid, 'Correct', UserDataInfo.Correct + 1)
            oprate_sql.UserDataUpdate(uid, 'Accuracy', int(((UserDataInfo.Correct + 1) * 100) / (UserDataInfo.Exercises + 1)))
            for us in userInfo:
                if int(QuizeID) == 1:
                    oprate_sql.UserDataUpdate(uid, 'Gold', us.Gold + 3)
                elif int(QuizeID) == 2:
                    oprate_sql.UserDataUpdate(uid, 'Gold', us.Gold + 5)
                elif int(QuizeID) == 3:
                    oprate_sql.UserDataUpdate(uid, 'Gold', us.Gold + 10)
        QuestionNum = CreateProblemNumber(QuestionCount, QuizeID-1)
        QuizzesInfo[1] = QuestionNum
        SetProList((QuizeID-1), DoCount[QuizeID-1], QuestionNum)
    QuizzesInfo[1] = QuestionNum
    print(DoCount[QuizeID-1])

    LockStatusArry = []
    for user in userInfo:
        LockStatus = user.QuizzesUnLock
    if GetDoCount(QuizeID-1) >= 9:
        LockStatus &= ((1 << (QuizeID-1)) ^ ( 0xFF ))
        print('LockStatus:%d'%LockStatus)
        DoCountClear(QuizeID-1)
        ClearList(QuizeID-1)
        oprate_sql.UserDataUpdate(uid, 'QuizzesUnLock', LockStatus)
    for i in range(QuiListCount):
        LockStatusArry.append((LockStatus >> i) & 0x1)

    UserDataList = UserDataInfoTable.query.filter_by(UserID=uid)
    for us in UserDataList:
        UserDataInfo = us

    return render_template('Emigrated.html',
                           QuizzesList=QuiList,
                           QuestionList=QuestionList,
                           QuestionNumber=QuestionNum,
                           QuizzesID=QuizeID,
                           msg=ErrorStatus,
                           LockStatus=LockStatusArry,
                           UserInfo=userInfou,
                           CosMsg=CosMsg,
                           UserDataInfo=UserDataInfo,
                           RankStatus=DataType.GetRankStatus(),
                           RankValue=DataType.GetRmarkValue())

def DoCountAdd(index):
    DoCount[index] += 1

def DoCountClear(index):
    DoCount[index] = 0

def GetDoCount(index):
    return DoCount[index]

def ClearList(index):
    for i in range(10):
        PrePorblemList[index][i] = 0

def SetProList(index, ind, value):
    PrePorblemList[index][ind] = value

def CreateProblemNumber(Count, index):
    Flag = 1
    subFlag = 1
    while(Flag):
        Num = random.randint(1, Count)
        if PrePorblemList[index].count(Num) > 0:
            Flag = 1
        else:
            Flag = 0
    print(PrePorblemList[index])
    print(Num)
    return Num
