from flask import request, render_template, Blueprint
from createdb import DiscussTable, ReplyTable, db

Blu_AdminDiscuss = Blueprint('Blu_AdminDiscuss', __name__)

global ShrinkStatus
ShrinkStatus = 0
@Blu_AdminDiscuss.route('/Admin_Discuss')
def Admin_Discuss():
    ShrinkS = request.args.get('Shrinkstatus')
    DisID = request.args.get('DisID')
    DiscussID = request.args.get('DiscussID')
    RepleyID = request.args.get('RepleyID')
    print(ShrinkS)
    print(DisID)
    if ShrinkS == None:
        ShrinkS = '1'
    if DisID == None:
        DisID = '1'
    DisID = int(DisID)
    global ShrinkStatus
    ShrinkStatus = int(ShrinkS)

    if ShrinkStatus == 0:
        ShrinkStatus = 1
    else:
        ShrinkStatus = 0
    print(ShrinkStatus)


    if DiscussID != None:
        DiscussID = int(DiscussID)
        DeleteDiscussList(DiscussID)
    if RepleyID != None:
        RepleyID = int(RepleyID)
        DeleteReplayList(RepleyID)
    DiscussList = DiscussTable.query.all()

    return render_template('Admin_Discuss.html',
                           HTML_DiscussList=DiscussList,
                           ShrinkStatus=ShrinkStatus,
                           DisID=DisID)

def DeleteDiscussList(id):
    repList = ReplyTable.query.filter_by(DiscussID=id)
    for re in repList:
        db.session.delete(re)
        db.session.commit()
    dis = DiscussTable.query.get(id)
    db.session.delete(dis)
    db.session.commit()

def DeleteReplayList(id):
    re = ReplyTable.query.get(id)
    db.session.delete(re)
    db.session.commit()