from flask import request, render_template, Blueprint, redirect
from apps import DataType, oprate_sql
from createdb import UserDataInfoTable, IndependentListTable, db, UserPublishListTable, UserInfoTable

Blu_Independent = Blueprint('Blu_Independent', __name__)
global ChoiceType
ChoiceType = 0
@Blu_Independent.route('/Independent', methods=['POST', 'GET'])
def Independent():

    Singout = request.args.get('Singout')
    if Singout != None:
        if int(Singout) == 1:
            DataType.ClearUserInfo()
    uid = DataType.GetUserInfo().id
    uid = str(uid)
    if (len(uid) == 0):
        return redirect('/Login')
    uid = int(uid)
    userInfo = oprate_sql.UserInfoCheckForID(uid)
    UserDataList = UserDataInfoTable.query.filter_by(UserID=uid)
    for us in UserDataList:
        UserDataInfo = us
    CType = request.args.get('ChoType')
    global ChoiceType
    if (CType != None) and (request.method != 'POST'):
        if ChoiceType == 1:
            ChoiceType = 0
        else:
            ChoiceType = 1
    if ChoiceType == 1:
        HTML = UserProblemList(userInfo, UserDataInfo)
    else:
        HTML = AdminProblemList(userInfo, UserDataInfo)
    return HTML
def IndependentLikes(id):
    In = IndependentListTable.query.get(id)
    In.Like += 1
    db.session.commit()
    if In.Like >= 50:
        oprate_sql.UserPublishListAdd(In.Content, In.AnswerDes, In.Answer, In.UserID)

def UserPublishListLikes(id):
    In = UserPublishListTable.query.get(id)
    In.Like += 1
    db.session.commit()
def AdminProblemList(userInfo, UserDataInfo):

    Result = ''
    OptionType = request.args.get("OptionType")
    if OptionType == None:
        OptionType = '0'
    OptionType = int(OptionType)
    print(OptionType)
    QuestionInfo = ''
    if OptionType == 1:
        ID = request.args.get('QuestionID')
        ID = int(ID)
        UserPublishListLikes(ID)
    elif OptionType == 2:
        ID = request.args.get('QuestionID')
        ID = int(ID)
        QuestionInfo = UserPublishListTable.query.get(ID)
        if request.method == 'POST':
            answer = request.form.get('Qanswer')
            if answer != QuestionInfo.Answer:
                Result = 'Wrong'
            else:
                Result = 'Correct'
    elif OptionType == 4:
        ID = request.args.get('QuestionID')
        ID = int(ID)
        QuestionInfo = UserPublishListTable.query.get(ID)
        if request.method == 'POST':
            Content = request.form.get('Question')
            Answer = request.form.get('Answer')

            oprate_sql.UserPublishListUpdate(ID, Content, '', Answer)
    IndependList = UserPublishListTable.query.all()
    AllUserList = UserInfoTable.query.all()
    return render_template('Independent.html',
                           UserInfo=userInfo,
                           UserDataInfo=UserDataInfo,
                           RankStatus=DataType.GetRankStatus(),
                           RankValue=DataType.GetRmarkValue(),
                           IndependList=IndependList,
                           OptionType=OptionType,
                           QuestionInfo=QuestionInfo,
                           Result=Result,
                           ChoType=ChoiceType,
                           AllUserList=AllUserList)

def UserProblemList(userInfo, UserDataInfo):
    ID = request.args.get('Likes')
    if ID != None:
        ID = int(ID)
        IndependentLikes(ID)
    if request.method == 'POST':
        Content = request.form.get('Question')
        oprate_sql.IndependentListAdd(Content, userInfo.id)
    IndependList = IndependentListTable.query.all()
    for inl in IndependList:
        if inl.Like >= 50:
            oprate_sql.UserPublishListAdd(inl.Content, inl.AnswerDes, inl.Answer, inl.UserID)
    AllUserList = UserInfoTable.query.all()
    return render_template('Independent.html',
                           UserInfo=userInfo,
                           UserDataInfo=UserDataInfo,
                           RankStatus=DataType.GetRankStatus(),
                           RankValue=DataType.GetRmarkValue(),
                           IndependList=IndependList,
                           ChoType=ChoiceType,
                           AllUserList=AllUserList)