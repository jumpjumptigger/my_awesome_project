from flask import render_template, request, Blueprint

Blu_UserPractice = Blueprint('Blu_UserPractice', __name__)

@Blu_UserPractice.route('/UserPractice', methods=['POST', 'GET'])
def UserPractice():
    render_template('UserPractice.html')