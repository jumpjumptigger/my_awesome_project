from flask import Flask
import Setting

app = Flask(__name__, template_folder='./templates', static_folder='./static')

app.config.from_object(Setting.DevelopmentConfig)

from apps.views import user, Study, Practice, Error, Discuss, Quizzes, \
    Personal, Admin_UserDefine, UserPractice, Administration, Admin_user, Admin_Discuss, Admin_Practice,\
    Admin_Knowledge, Independent, Admin_Quizzes, Head
app.register_blueprint(user.Blu_user)
app.register_blueprint(Study.Blu_study)
app.register_blueprint(Practice.Blu_practice)
app.register_blueprint(Error.Blu_Error)
app.register_blueprint(Discuss.Blu_Discuss)
app.register_blueprint(Quizzes.Blu_Quizzes)
app.register_blueprint(Personal.Blu_Personal)
app.register_blueprint(Admin_UserDefine.Blu_AdminUserDefine)
app.register_blueprint(Administration.Blu_Admin)
app.register_blueprint(Admin_user.Blu_AdminUser)
app.register_blueprint(UserPractice.Blu_UserPractice)
app.register_blueprint(Admin_Discuss.Blu_AdminDiscuss)
app.register_blueprint(Admin_Practice.Blu_AdminPractice)
app.register_blueprint(Admin_Knowledge.Blu_AdminKnowledge)
app.register_blueprint(Independent.Blu_Independent)
app.register_blueprint(Admin_Quizzes.Blu_AdminQuizzes)
app.register_blueprint(Head.Blu_Header)
